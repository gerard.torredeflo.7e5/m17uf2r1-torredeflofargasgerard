﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockPlayerMovement : StateMachineBehaviour
{
    private GameObject player;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<PlayerControls>().movementBlocked = true;
        player.GetComponent<PlayerControls>().VictoryCamOn();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        player.GetComponent<PlayerControls>().movementBlocked = false;
        player.GetComponent<PlayerControls>().VictoryCamOff();
    }
}
