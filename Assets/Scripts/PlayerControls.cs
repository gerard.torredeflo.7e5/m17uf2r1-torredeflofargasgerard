﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    private CharacterController characterController;
    private Animator animator;
    [SerializeField] private GameObject mainCam;
    [SerializeField] private GameObject shootCam;
    [SerializeField] private GameObject hoorayCam;

    [SerializeField] private float speed = 6.0f;
    [SerializeField] private float jumpSpeed = 8.0f;
    [SerializeField] private float gravity = 20.0f;

    public bool movementBlocked = false;

    private Vector3 moveDirection = Vector3.zero;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (characterController.isGrounded & !movementBlocked)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection *= speed;
            var v = Input.GetAxis("Vertical");
            animator.SetFloat("Speed", v);
            //moveDirection.y -= gravity * Time.deltaTime;
            Interact();
            Jump();
            Crouch();
            Celebrate();
            Shooting();

        }
        moveDirection.y -= gravity * Time.deltaTime;
        characterController.Move(moveDirection * Time.deltaTime);
    }

    private void Shooting()
    {
        if (Input.GetMouseButton(0))
        {
            animator.SetBool("Shooting", true);
            mainCam.SetActive(false);
            shootCam.SetActive(true);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            animator.SetBool("Shooting", false);
            mainCam.SetActive(true);
            shootCam.SetActive(false);
        }
    }

    private void Celebrate()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            animator.SetTrigger("Victory");
        }
    }

    public void VictoryCamOn()
    {
        mainCam.SetActive(false);
        shootCam.SetActive(false);
        hoorayCam.SetActive(true);
    }

    public void VictoryCamOff()
    {
        mainCam.SetActive(true);
        shootCam.SetActive(false);
        hoorayCam.SetActive(false);
    }

    private void Interact()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            animator.SetTrigger("Interact");
        }
    }

    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            animator.SetTrigger("Jump");
        }
    }

    private void Crouch()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (animator.GetBool("Crouch"))
            {
                animator.SetBool("Crouch", false);
            }
            else if (!animator.GetBool("Crouch"))
            {
                animator.SetBool("Crouch", true);
            }
        }
    }
}